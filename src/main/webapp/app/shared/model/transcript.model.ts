import { IStudent } from 'app/shared/model/student.model';

export interface ITranscript {
  id?: number;
  passed?: boolean;
  grade?: number;
  student?: IStudent;
}

export const defaultValue: Readonly<ITranscript> = {
  passed: false,
};
