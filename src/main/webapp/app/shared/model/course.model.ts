import { ICourseCategory } from 'app/shared/model/course-category.model';

export interface ICourse {
  id?: number;
  name?: string;
  description?: string | null;
  prerequisite?: string | null;
  imageContentType?: string | null;
  image?: string | null;
  courseCategory?: ICourseCategory | null;
}

export const defaultValue: Readonly<ICourse> = {};
