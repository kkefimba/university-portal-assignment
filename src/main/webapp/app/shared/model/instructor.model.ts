export interface IInstructor {
  id?: number;
  fullName?: string;
}

export const defaultValue: Readonly<IInstructor> = {};
