import { ICourse } from 'app/shared/model/course.model';

export interface ICourseCategory {
  id?: number;
  name?: string;
  description?: string | null;
  products?: ICourse[] | null;
}

export const defaultValue: Readonly<ICourseCategory> = {};
