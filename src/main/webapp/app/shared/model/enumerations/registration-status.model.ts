export enum RegistrationStatus {
  COMPLETED = 'COMPLETED',

  PENDING = 'PENDING',

  CANCELLED = 'CANCELLED',
}
