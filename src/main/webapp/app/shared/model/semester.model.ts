export interface ISemester {
  id?: number;
  semesterCode?: string | null;
  semesterText?: string | null;
}

export const defaultValue: Readonly<ISemester> = {};
