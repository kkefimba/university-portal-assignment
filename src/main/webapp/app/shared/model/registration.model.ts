import dayjs from 'dayjs';
import { ISemester } from 'app/shared/model/semester.model';
import { ICourse } from 'app/shared/model/course.model';
import { IStudent } from 'app/shared/model/student.model';
import { RegistrationStatus } from 'app/shared/model/enumerations/registration-status.model';

export interface IRegistration {
  id?: number;
  registrationDate?: string;
  status?: RegistrationStatus;
  semester?: ISemester;
  course?: ICourse;
  student?: IStudent;
}

export const defaultValue: Readonly<IRegistration> = {};
