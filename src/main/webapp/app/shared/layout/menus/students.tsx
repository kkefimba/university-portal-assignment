import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { Translate, translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown
    icon="th-list"
    name="Actions"
    id="entity-menu"
    data-cy="entity"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <>{/* to avoid warnings when empty */}</>
    <MenuItem icon="asterisk" to="/student">
      <Translate contentKey="global.menu.entities.student" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/transcript">
      <Translate contentKey="global.menu.entities.transcript" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/course">
      <Translate contentKey="global.menu.entities.course" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/course-category">
      <Translate contentKey="global.menu.entities.courseCategory" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/registration">
      <Translate contentKey="global.menu.entities.registration" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/semester">
      <Translate contentKey="global.menu.entities.semester" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/instructor">
      <Translate contentKey="global.menu.entities.instructor" />
    </MenuItem>
  </NavDropdown>
);
