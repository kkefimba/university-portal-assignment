import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './transcript.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const TranscriptDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const transcriptEntity = useAppSelector(state => state.transcript.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="transcriptDetailsHeading">
          <Translate contentKey="portalApp.transcript.detail.title">Transcript</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{transcriptEntity.id}</dd>
          <dt>
            <span id="passed">
              <Translate contentKey="portalApp.transcript.passed">Passed</Translate>
            </span>
          </dt>
          <dd>{transcriptEntity.passed ? 'true' : 'false'}</dd>
          <dt>
            <span id="grade">
              <Translate contentKey="portalApp.transcript.grade">Grade</Translate>
            </span>
          </dt>
          <dd>{transcriptEntity.grade}</dd>
          <dt>
            <Translate contentKey="portalApp.transcript.student">Student</Translate>
          </dt>
          <dd>{transcriptEntity.student ? transcriptEntity.student.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/transcript" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/transcript/${transcriptEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default TranscriptDetail;
