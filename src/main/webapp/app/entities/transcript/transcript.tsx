import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntities } from './transcript.reducer';
import { ITranscript } from 'app/shared/model/transcript.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const Transcript = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const transcriptList = useAppSelector(state => state.transcript.entities);
  const loading = useAppSelector(state => state.transcript.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="transcript-heading" data-cy="TranscriptHeading">
        <Translate contentKey="portalApp.transcript.home.title">Transcripts</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="portalApp.transcript.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="portalApp.transcript.home.createLabel">Create new Transcript</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {transcriptList && transcriptList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="portalApp.transcript.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="portalApp.transcript.passed">Passed</Translate>
                </th>
                <th>
                  <Translate contentKey="portalApp.transcript.grade">Grade</Translate>
                </th>
                <th>
                  <Translate contentKey="portalApp.transcript.student">Student</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {transcriptList.map((transcript, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${transcript.id}`} color="link" size="sm">
                      {transcript.id}
                    </Button>
                  </td>
                  <td>{transcript.passed ? 'true' : 'false'}</td>
                  <td>{transcript.grade}</td>
                  <td>{transcript.student ? <Link to={`student/${transcript.student.id}`}>{transcript.student.id}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${transcript.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${transcript.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${transcript.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="portalApp.transcript.home.notFound">No Transcripts found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default Transcript;
