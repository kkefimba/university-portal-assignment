import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './semester.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const SemesterDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const semesterEntity = useAppSelector(state => state.semester.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="semesterDetailsHeading">
          <Translate contentKey="portalApp.semester.detail.title">Semester</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{semesterEntity.id}</dd>
          <dt>
            <span id="semesterCode">
              <Translate contentKey="portalApp.semester.semesterCode">Semester Code</Translate>
            </span>
          </dt>
          <dd>{semesterEntity.semesterCode}</dd>
          <dt>
            <span id="semesterText">
              <Translate contentKey="portalApp.semester.semesterText">Semester Text</Translate>
            </span>
          </dt>
          <dd>{semesterEntity.semesterText}</dd>
        </dl>
        <Button tag={Link} to="/semester" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/semester/${semesterEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default SemesterDetail;
