import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Student from './student';
import Transcript from './transcript';
import Course from './course';
import CourseCategory from './course-category';
import Registration from './registration';
import Semester from './semester';
import Instructor from './instructor';

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}student`} component={Student} />
      <ErrorBoundaryRoute path={`${match.url}transcript`} component={Transcript} />
      <ErrorBoundaryRoute path={`${match.url}course`} component={Course} />
      <ErrorBoundaryRoute path={`${match.url}course-category`} component={CourseCategory} />
      <ErrorBoundaryRoute path={`${match.url}registration`} component={Registration} />
      <ErrorBoundaryRoute path={`${match.url}semester`} component={Semester} />
      <ErrorBoundaryRoute path={`${match.url}instructor`} component={Instructor} />
    </Switch>
  </div>
);

export default Routes;
