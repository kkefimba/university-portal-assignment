import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './registration.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const RegistrationDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const registrationEntity = useAppSelector(state => state.registration.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="registrationDetailsHeading">
          <Translate contentKey="portalApp.registration.detail.title">Registration</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{registrationEntity.id}</dd>
          <dt>
            <span id="registrationDate">
              <Translate contentKey="portalApp.registration.registrationDate">Registration Date</Translate>
            </span>
          </dt>
          <dd>
            {registrationEntity.registrationDate ? (
              <TextFormat value={registrationEntity.registrationDate} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="status">
              <Translate contentKey="portalApp.registration.status">Status</Translate>
            </span>
          </dt>
          <dd>{registrationEntity.status}</dd>
          <dt>
            <Translate contentKey="portalApp.registration.semester">Semester</Translate>
          </dt>
          <dd>{registrationEntity.semester ? registrationEntity.semester.id : ''}</dd>
          <dt>
            <Translate contentKey="portalApp.registration.course">Course</Translate>
          </dt>
          <dd>{registrationEntity.course ? registrationEntity.course.name : ''}</dd>
          <dt>
            <Translate contentKey="portalApp.registration.student">Student</Translate>
          </dt>
          <dd>{registrationEntity.student ? registrationEntity.student.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/registration" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/registration/${registrationEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default RegistrationDetail;
