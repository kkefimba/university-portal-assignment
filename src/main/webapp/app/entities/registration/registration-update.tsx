import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ISemester } from 'app/shared/model/semester.model';
import { getEntities as getSemesters } from 'app/entities/semester/semester.reducer';
import { ICourse } from 'app/shared/model/course.model';
import { getEntities as getCourses } from 'app/entities/course/course.reducer';
import { IStudent } from 'app/shared/model/student.model';
import { getEntities as getStudents } from 'app/entities/student/student.reducer';
import { getEntity, updateEntity, createEntity, reset } from './registration.reducer';
import { IRegistration } from 'app/shared/model/registration.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const RegistrationUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const semesters = useAppSelector(state => state.semester.entities);
  const courses = useAppSelector(state => state.course.entities);
  const students = useAppSelector(state => state.student.entities);
  const registrationEntity = useAppSelector(state => state.registration.entity);
  const loading = useAppSelector(state => state.registration.loading);
  const updating = useAppSelector(state => state.registration.updating);
  const updateSuccess = useAppSelector(state => state.registration.updateSuccess);

  const handleClose = () => {
    props.history.push('/registration' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getSemesters({}));
    dispatch(getCourses({}));
    dispatch(getStudents({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.registrationDate = convertDateTimeToServer(values.registrationDate);

    const entity = {
      ...registrationEntity,
      ...values,
      semester: semesters.find(it => it.id.toString() === values.semesterId.toString()),
      course: courses.find(it => it.id.toString() === values.courseId.toString()),
      student: students.find(it => it.id.toString() === values.studentId.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          registrationDate: displayDefaultDateTime(),
        }
      : {
          ...registrationEntity,
          registrationDate: convertDateTimeFromServer(registrationEntity.registrationDate),
          status: 'COMPLETED',
          semesterId: registrationEntity?.semester?.id,
          courseId: registrationEntity?.course?.id,
          studentId: registrationEntity?.student?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="portalApp.registration.home.createOrEditLabel" data-cy="RegistrationCreateUpdateHeading">
            <Translate contentKey="portalApp.registration.home.createOrEditLabel">Create or edit a Registration</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="registration-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('portalApp.registration.registrationDate')}
                id="registration-registrationDate"
                name="registrationDate"
                data-cy="registrationDate"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('portalApp.registration.status')}
                id="registration-status"
                name="status"
                data-cy="status"
                type="select"
              >
                <option value="COMPLETED">{translate('portalApp.RegistrationStatus.COMPLETED')}</option>
                <option value="PENDING">{translate('portalApp.RegistrationStatus.PENDING')}</option>
                <option value="CANCELLED">{translate('portalApp.RegistrationStatus.CANCELLED')}</option>
              </ValidatedField>
              <ValidatedField
                id="registration-semester"
                name="semesterId"
                data-cy="semester"
                label={translate('portalApp.registration.semester')}
                type="select"
                required
              >
                <option value="" key="0" />
                {semesters
                  ? semesters.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <FormText>
                <Translate contentKey="entity.validation.required">This field is required.</Translate>
              </FormText>
              <ValidatedField
                id="registration-course"
                name="courseId"
                data-cy="course"
                label={translate('portalApp.registration.course')}
                type="select"
                required
              >
                <option value="" key="0" />
                {courses
                  ? courses.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <FormText>
                <Translate contentKey="entity.validation.required">This field is required.</Translate>
              </FormText>
              <ValidatedField
                id="registration-student"
                name="studentId"
                data-cy="student"
                label={translate('portalApp.registration.student')}
                type="select"
                required
              >
                <option value="" key="0" />
                {students
                  ? students.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <FormText>
                <Translate contentKey="entity.validation.required">This field is required.</Translate>
              </FormText>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/registration" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default RegistrationUpdate;
