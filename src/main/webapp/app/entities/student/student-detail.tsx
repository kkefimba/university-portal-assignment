import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './student.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const StudentDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const studentEntity = useAppSelector(state => state.student.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="studentDetailsHeading">
          <Translate contentKey="portalApp.student.detail.title">Student</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{studentEntity.id}</dd>
          <dt>
            <span id="firstName">
              <Translate contentKey="portalApp.student.firstName">First Name</Translate>
            </span>
          </dt>
          <dd>{studentEntity.firstName}</dd>
          <dt>
            <span id="lastName">
              <Translate contentKey="portalApp.student.lastName">Last Name</Translate>
            </span>
          </dt>
          <dd>{studentEntity.lastName}</dd>
          <dt>
            <span id="gender">
              <Translate contentKey="portalApp.student.gender">Gender</Translate>
            </span>
          </dt>
          <dd>{studentEntity.gender}</dd>
          <dt>
            <span id="email">
              <Translate contentKey="portalApp.student.email">Email</Translate>
            </span>
          </dt>
          <dd>{studentEntity.email}</dd>
          <dt>
            <span id="phone">
              <Translate contentKey="portalApp.student.phone">Phone</Translate>
            </span>
          </dt>
          <dd>{studentEntity.phone}</dd>
          <dt>
            <span id="addressLine1">
              <Translate contentKey="portalApp.student.addressLine1">Address Line 1</Translate>
            </span>
          </dt>
          <dd>{studentEntity.addressLine1}</dd>
          <dt>
            <span id="addressLine2">
              <Translate contentKey="portalApp.student.addressLine2">Address Line 2</Translate>
            </span>
          </dt>
          <dd>{studentEntity.addressLine2}</dd>
          <dt>
            <span id="city">
              <Translate contentKey="portalApp.student.city">City</Translate>
            </span>
          </dt>
          <dd>{studentEntity.city}</dd>
          <dt>
            <span id="country">
              <Translate contentKey="portalApp.student.country">Country</Translate>
            </span>
          </dt>
          <dd>{studentEntity.country}</dd>
          <dt>
            <Translate contentKey="portalApp.student.user">User</Translate>
          </dt>
          <dd>{studentEntity.user ? studentEntity.user.login : ''}</dd>
        </dl>
        <Button tag={Link} to="/student" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/student/${studentEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default StudentDetail;
