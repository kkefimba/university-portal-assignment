/**
 * View Models used by Spring MVC REST controllers.
 */
package com.university.portal.web.rest.vm;
