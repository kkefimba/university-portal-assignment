package com.university.portal.service;

import com.university.portal.domain.CourseCategory;
import com.university.portal.repository.CourseCategoryRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class CourseCategoryService {

    private final Logger log = LoggerFactory.getLogger(CourseCategoryService.class);

    private final CourseCategoryRepository courseCategoryRepository;

    public CourseCategoryService(CourseCategoryRepository courseCategoryRepository) {
        this.courseCategoryRepository = courseCategoryRepository;
    }

    /**
     * Save a courseCategory.
     *
     * @param courseCategory the entity to save.
     * @return the persisted entity.
     */
    public CourseCategory save(CourseCategory courseCategory) {
        log.debug("Request to save CourseCategory : {}", courseCategory);
        return courseCategoryRepository.save(courseCategory);
    }

    /**
     * Partially update a courseCategory.
     *
     * @param courseCategory the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CourseCategory> partialUpdate(CourseCategory courseCategory) {
        log.debug("Request to partially update CourseCategory : {}", courseCategory);

        return courseCategoryRepository
            .findById(courseCategory.getId())
            .map(
                existingCourseCategory -> {
                    if (courseCategory.getName() != null) {
                        existingCourseCategory.setName(courseCategory.getName());
                    }
                    if (courseCategory.getDescription() != null) {
                        existingCourseCategory.setDescription(courseCategory.getDescription());
                    }

                    return existingCourseCategory;
                }
            )
            .map(courseCategoryRepository::save);
    }

    /**
     * Get all the courseCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CourseCategory> findAll(Pageable pageable) {
        log.debug("Request to get all CourseCategories");
        return courseCategoryRepository.findAll(pageable);
    }

    /**
     * Get one courseCategory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CourseCategory> findOne(Long id) {
        log.debug("Request to get CourseCategory : {}", id);
        return courseCategoryRepository.findById(id);
    }

    /**
     * Delete the courseCategory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CourseCategory : {}", id);
        courseCategoryRepository.deleteById(id);
    }
}
