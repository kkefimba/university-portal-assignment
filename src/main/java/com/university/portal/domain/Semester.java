package com.university.portal.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * A Semester.
 */
@Entity
@Table(name = "semester")
public class Semester implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "semester_code")
    private String semesterCode;

    @Column(name = "semester_text")
    private String semesterText;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Semester id(Long id) {
        this.id = id;
        return this;
    }

    public String getSemesterCode() {
        return this.semesterCode;
    }

    public Semester semesterCode(String semesterCode) {
        this.semesterCode = semesterCode;
        return this;
    }

    public void setSemesterCode(String semesterCode) {
        this.semesterCode = semesterCode;
    }

    public String getSemesterText() {
        return this.semesterText;
    }

    public Semester semesterText(String semesterText) {
        this.semesterText = semesterText;
        return this;
    }

    public void setSemesterText(String semesterText) {
        this.semesterText = semesterText;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Semester)) {
            return false;
        }
        return id != null && id.equals(((Semester) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "Semester{" +
            "id=" + getId() +
            ", semesterCode='" + getSemesterCode() + "'" +
            ", semesterText='" + getSemesterText() + "'" +
            "}";
    }
}
