package com.university.portal.domain.enumeration;

/**
 * The RegistrationStatus enumeration.
 */
public enum RegistrationStatus {
    COMPLETED,
    PENDING,
    CANCELLED,
}
