package com.university.portal.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE,
    FEMALE,
    OTHER,
}
