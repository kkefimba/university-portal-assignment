package com.university.portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A CourseCategory.
 */
@Entity
@Table(name = "course_category")
public class CourseCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "courseCategory")
    @JsonIgnoreProperties(value = { "courseCategory" }, allowSetters = true)
    private Set<Course> products = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CourseCategory id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public CourseCategory name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public CourseCategory description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Course> getProducts() {
        return this.products;
    }

    public CourseCategory products(Set<Course> courses) {
        this.setProducts(courses);
        return this;
    }

    public CourseCategory addProduct(Course course) {
        this.products.add(course);
        course.setCourseCategory(this);
        return this;
    }

    public CourseCategory removeProduct(Course course) {
        this.products.remove(course);
        course.setCourseCategory(null);
        return this;
    }

    public void setProducts(Set<Course> courses) {
        if (this.products != null) {
            this.products.forEach(i -> i.setCourseCategory(null));
        }
        if (courses != null) {
            courses.forEach(i -> i.setCourseCategory(this));
        }
        this.products = courses;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CourseCategory)) {
            return false;
        }
        return id != null && id.equals(((CourseCategory) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "CourseCategory{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
