package com.university.portal.cucumber;

import com.university.portal.PortalApp;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = PortalApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
