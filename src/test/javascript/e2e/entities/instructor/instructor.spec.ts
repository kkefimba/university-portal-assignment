import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import InstructorComponentsPage from './instructor.page-object';
import InstructorUpdatePage from './instructor-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('Instructor e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let instructorComponentsPage: InstructorComponentsPage;
  let instructorUpdatePage: InstructorUpdatePage;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();
    await signInPage.username.sendKeys(username);
    await signInPage.password.sendKeys(password);
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    instructorComponentsPage = new InstructorComponentsPage();
    instructorComponentsPage = await instructorComponentsPage.goToPage(navBarPage);
  });

  it('should load Instructors', async () => {
    expect(await instructorComponentsPage.title.getText()).to.match(/Instructors/);
    expect(await instructorComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete Instructors', async () => {
    const beforeRecordsCount = (await isVisible(instructorComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(instructorComponentsPage.table);
    instructorUpdatePage = await instructorComponentsPage.goToCreateInstructor();
    await instructorUpdatePage.enterData();
    expect(await isVisible(instructorUpdatePage.saveButton)).to.be.false;

    expect(await instructorComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(instructorComponentsPage.table);
    await waitUntilCount(instructorComponentsPage.records, beforeRecordsCount + 1);
    expect(await instructorComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await instructorComponentsPage.deleteInstructor();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(instructorComponentsPage.records, beforeRecordsCount);
      expect(await instructorComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(instructorComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
