import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import InstructorUpdatePage from './instructor-update.page-object';

const expect = chai.expect;
export class InstructorDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('portalApp.instructor.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-instructor'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class InstructorComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('instructor-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('instructor');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateInstructor() {
    await this.createButton.click();
    return new InstructorUpdatePage();
  }

  async deleteInstructor() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const instructorDeleteDialog = new InstructorDeleteDialog();
    await waitUntilDisplayed(instructorDeleteDialog.deleteModal);
    expect(await instructorDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/portalApp.instructor.delete.question/);
    await instructorDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(instructorDeleteDialog.deleteModal);

    expect(await isVisible(instructorDeleteDialog.deleteModal)).to.be.false;
  }
}
