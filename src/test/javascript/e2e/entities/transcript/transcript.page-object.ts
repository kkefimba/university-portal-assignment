import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import TranscriptUpdatePage from './transcript-update.page-object';

const expect = chai.expect;
export class TranscriptDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('portalApp.transcript.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-transcript'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class TranscriptComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('transcript-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('transcript');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateTranscript() {
    await this.createButton.click();
    return new TranscriptUpdatePage();
  }

  async deleteTranscript() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const transcriptDeleteDialog = new TranscriptDeleteDialog();
    await waitUntilDisplayed(transcriptDeleteDialog.deleteModal);
    expect(await transcriptDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/portalApp.transcript.delete.question/);
    await transcriptDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(transcriptDeleteDialog.deleteModal);

    expect(await isVisible(transcriptDeleteDialog.deleteModal)).to.be.false;
  }
}
