import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import TranscriptComponentsPage from './transcript.page-object';
import TranscriptUpdatePage from './transcript-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('Transcript e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let transcriptComponentsPage: TranscriptComponentsPage;
  let transcriptUpdatePage: TranscriptUpdatePage;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();
    await signInPage.username.sendKeys(username);
    await signInPage.password.sendKeys(password);
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    transcriptComponentsPage = new TranscriptComponentsPage();
    transcriptComponentsPage = await transcriptComponentsPage.goToPage(navBarPage);
  });

  it('should load Transcripts', async () => {
    expect(await transcriptComponentsPage.title.getText()).to.match(/Transcripts/);
    expect(await transcriptComponentsPage.createButton.isEnabled()).to.be.true;
  });

  /* it('should create and delete Transcripts', async () => {
        const beforeRecordsCount = await isVisible(transcriptComponentsPage.noRecords) ? 0 : await getRecordsCount(transcriptComponentsPage.table);
        transcriptUpdatePage = await transcriptComponentsPage.goToCreateTranscript();
        await transcriptUpdatePage.enterData();
        expect(await isVisible(transcriptUpdatePage.saveButton)).to.be.false;

        expect(await transcriptComponentsPage.createButton.isEnabled()).to.be.true;
        await waitUntilDisplayed(transcriptComponentsPage.table);
        await waitUntilCount(transcriptComponentsPage.records, beforeRecordsCount + 1);
        expect(await transcriptComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

        await transcriptComponentsPage.deleteTranscript();
        if(beforeRecordsCount !== 0) {
          await waitUntilCount(transcriptComponentsPage.records, beforeRecordsCount);
          expect(await transcriptComponentsPage.records.count()).to.eq(beforeRecordsCount);
        } else {
          await waitUntilDisplayed(transcriptComponentsPage.noRecords);
        }
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
