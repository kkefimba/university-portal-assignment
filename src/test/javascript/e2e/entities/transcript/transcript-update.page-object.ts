import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class TranscriptUpdatePage {
  pageTitle: ElementFinder = element(by.id('portalApp.transcript.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  passedInput: ElementFinder = element(by.css('input#transcript-passed'));
  gradeInput: ElementFinder = element(by.css('input#transcript-grade'));
  studentSelect: ElementFinder = element(by.css('select#transcript-student'));

  getPageTitle() {
    return this.pageTitle;
  }

  getPassedInput() {
    return this.passedInput;
  }
  async setGradeInput(grade) {
    await this.gradeInput.sendKeys(grade);
  }

  async getGradeInput() {
    return this.gradeInput.getAttribute('value');
  }

  async studentSelectLastOption() {
    await this.studentSelect.all(by.tagName('option')).last().click();
  }

  async studentSelectOption(option) {
    await this.studentSelect.sendKeys(option);
  }

  getStudentSelect() {
    return this.studentSelect;
  }

  async getStudentSelectedOption() {
    return this.studentSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    const selectedPassed = await this.getPassedInput().isSelected();
    if (selectedPassed) {
      await this.getPassedInput().click();
    } else {
      await this.getPassedInput().click();
    }
    await waitUntilDisplayed(this.saveButton);
    await this.setGradeInput('5');
    await this.studentSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
  }
}
