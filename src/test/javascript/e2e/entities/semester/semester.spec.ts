import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import SemesterComponentsPage from './semester.page-object';
import SemesterUpdatePage from './semester-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('Semester e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let semesterComponentsPage: SemesterComponentsPage;
  let semesterUpdatePage: SemesterUpdatePage;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();
    await signInPage.username.sendKeys(username);
    await signInPage.password.sendKeys(password);
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    semesterComponentsPage = new SemesterComponentsPage();
    semesterComponentsPage = await semesterComponentsPage.goToPage(navBarPage);
  });

  it('should load Semesters', async () => {
    expect(await semesterComponentsPage.title.getText()).to.match(/Semesters/);
    expect(await semesterComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete Semesters', async () => {
    const beforeRecordsCount = (await isVisible(semesterComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(semesterComponentsPage.table);
    semesterUpdatePage = await semesterComponentsPage.goToCreateSemester();
    await semesterUpdatePage.enterData();
    expect(await isVisible(semesterUpdatePage.saveButton)).to.be.false;

    expect(await semesterComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(semesterComponentsPage.table);
    await waitUntilCount(semesterComponentsPage.records, beforeRecordsCount + 1);
    expect(await semesterComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await semesterComponentsPage.deleteSemester();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(semesterComponentsPage.records, beforeRecordsCount);
      expect(await semesterComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(semesterComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
