import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import SemesterUpdatePage from './semester-update.page-object';

const expect = chai.expect;
export class SemesterDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('portalApp.semester.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-semester'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class SemesterComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('semester-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('semester');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateSemester() {
    await this.createButton.click();
    return new SemesterUpdatePage();
  }

  async deleteSemester() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const semesterDeleteDialog = new SemesterDeleteDialog();
    await waitUntilDisplayed(semesterDeleteDialog.deleteModal);
    expect(await semesterDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/portalApp.semester.delete.question/);
    await semesterDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(semesterDeleteDialog.deleteModal);

    expect(await isVisible(semesterDeleteDialog.deleteModal)).to.be.false;
  }
}
