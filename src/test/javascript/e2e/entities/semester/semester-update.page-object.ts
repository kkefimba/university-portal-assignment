import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class SemesterUpdatePage {
  pageTitle: ElementFinder = element(by.id('portalApp.semester.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  semesterCodeInput: ElementFinder = element(by.css('input#semester-semesterCode'));
  semesterTextInput: ElementFinder = element(by.css('input#semester-semesterText'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setSemesterCodeInput(semesterCode) {
    await this.semesterCodeInput.sendKeys(semesterCode);
  }

  async getSemesterCodeInput() {
    return this.semesterCodeInput.getAttribute('value');
  }

  async setSemesterTextInput(semesterText) {
    await this.semesterTextInput.sendKeys(semesterText);
  }

  async getSemesterTextInput() {
    return this.semesterTextInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setSemesterCodeInput('semesterCode');
    await waitUntilDisplayed(this.saveButton);
    await this.setSemesterTextInput('semesterText');
    await this.save();
    await waitUntilHidden(this.saveButton);
  }
}
