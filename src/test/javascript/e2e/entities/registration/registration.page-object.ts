import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import RegistrationUpdatePage from './registration-update.page-object';

const expect = chai.expect;
export class RegistrationDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('portalApp.registration.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-registration'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class RegistrationComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('registration-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('registration');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateRegistration() {
    await this.createButton.click();
    return new RegistrationUpdatePage();
  }

  async deleteRegistration() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const registrationDeleteDialog = new RegistrationDeleteDialog();
    await waitUntilDisplayed(registrationDeleteDialog.deleteModal);
    expect(await registrationDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/portalApp.registration.delete.question/);
    await registrationDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(registrationDeleteDialog.deleteModal);

    expect(await isVisible(registrationDeleteDialog.deleteModal)).to.be.false;
  }
}
