import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import RegistrationComponentsPage from './registration.page-object';
import RegistrationUpdatePage from './registration-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('Registration e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let registrationComponentsPage: RegistrationComponentsPage;
  let registrationUpdatePage: RegistrationUpdatePage;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();
    await signInPage.username.sendKeys(username);
    await signInPage.password.sendKeys(password);
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    registrationComponentsPage = new RegistrationComponentsPage();
    registrationComponentsPage = await registrationComponentsPage.goToPage(navBarPage);
  });

  it('should load Registrations', async () => {
    expect(await registrationComponentsPage.title.getText()).to.match(/Registrations/);
    expect(await registrationComponentsPage.createButton.isEnabled()).to.be.true;
  });

  /* it('should create and delete Registrations', async () => {
        const beforeRecordsCount = await isVisible(registrationComponentsPage.noRecords) ? 0 : await getRecordsCount(registrationComponentsPage.table);
        registrationUpdatePage = await registrationComponentsPage.goToCreateRegistration();
        await registrationUpdatePage.enterData();
        expect(await isVisible(registrationUpdatePage.saveButton)).to.be.false;

        expect(await registrationComponentsPage.createButton.isEnabled()).to.be.true;
        await waitUntilDisplayed(registrationComponentsPage.table);
        await waitUntilCount(registrationComponentsPage.records, beforeRecordsCount + 1);
        expect(await registrationComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

        await registrationComponentsPage.deleteRegistration();
        if(beforeRecordsCount !== 0) {
          await waitUntilCount(registrationComponentsPage.records, beforeRecordsCount);
          expect(await registrationComponentsPage.records.count()).to.eq(beforeRecordsCount);
        } else {
          await waitUntilDisplayed(registrationComponentsPage.noRecords);
        }
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
