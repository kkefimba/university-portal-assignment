import { element, by, ElementFinder, protractor } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class RegistrationUpdatePage {
  pageTitle: ElementFinder = element(by.id('portalApp.registration.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  registrationDateInput: ElementFinder = element(by.css('input#registration-registrationDate'));
  statusSelect: ElementFinder = element(by.css('select#registration-status'));
  semesterSelect: ElementFinder = element(by.css('select#registration-semester'));
  courseSelect: ElementFinder = element(by.css('select#registration-course'));
  studentSelect: ElementFinder = element(by.css('select#registration-student'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setRegistrationDateInput(registrationDate) {
    await this.registrationDateInput.sendKeys(registrationDate);
  }

  async getRegistrationDateInput() {
    return this.registrationDateInput.getAttribute('value');
  }

  async setStatusSelect(status) {
    await this.statusSelect.sendKeys(status);
  }

  async getStatusSelect() {
    return this.statusSelect.element(by.css('option:checked')).getText();
  }

  async statusSelectLastOption() {
    await this.statusSelect.all(by.tagName('option')).last().click();
  }
  async semesterSelectLastOption() {
    await this.semesterSelect.all(by.tagName('option')).last().click();
  }

  async semesterSelectOption(option) {
    await this.semesterSelect.sendKeys(option);
  }

  getSemesterSelect() {
    return this.semesterSelect;
  }

  async getSemesterSelectedOption() {
    return this.semesterSelect.element(by.css('option:checked')).getText();
  }

  async courseSelectLastOption() {
    await this.courseSelect.all(by.tagName('option')).last().click();
  }

  async courseSelectOption(option) {
    await this.courseSelect.sendKeys(option);
  }

  getCourseSelect() {
    return this.courseSelect;
  }

  async getCourseSelectedOption() {
    return this.courseSelect.element(by.css('option:checked')).getText();
  }

  async studentSelectLastOption() {
    await this.studentSelect.all(by.tagName('option')).last().click();
  }

  async studentSelectOption(option) {
    await this.studentSelect.sendKeys(option);
  }

  getStudentSelect() {
    return this.studentSelect;
  }

  async getStudentSelectedOption() {
    return this.studentSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setRegistrationDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM');
    await waitUntilDisplayed(this.saveButton);
    await this.statusSelectLastOption();
    await this.semesterSelectLastOption();
    await this.courseSelectLastOption();
    await this.studentSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
  }
}
