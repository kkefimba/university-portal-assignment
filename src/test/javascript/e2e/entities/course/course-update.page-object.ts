import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

import path from 'path';

const expect = chai.expect;

const fileToUpload = '../../../../../../src/main/webapp/content/images/logo-jhipster.png';
const absolutePath = path.resolve(__dirname, fileToUpload);
export default class CourseUpdatePage {
  pageTitle: ElementFinder = element(by.id('portalApp.course.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#course-name'));
  descriptionInput: ElementFinder = element(by.css('input#course-description'));
  prerequisiteInput: ElementFinder = element(by.css('input#course-prerequisite'));
  imageInput: ElementFinder = element(by.css('input#course-image'));
  courseCategorySelect: ElementFinder = element(by.css('select#course-courseCategory'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  async setDescriptionInput(description) {
    await this.descriptionInput.sendKeys(description);
  }

  async getDescriptionInput() {
    return this.descriptionInput.getAttribute('value');
  }

  async setPrerequisiteInput(prerequisite) {
    await this.prerequisiteInput.sendKeys(prerequisite);
  }

  async getPrerequisiteInput() {
    return this.prerequisiteInput.getAttribute('value');
  }

  async setImageInput(image) {
    await this.imageInput.sendKeys(image);
  }

  async getImageInput() {
    return this.imageInput.getAttribute('value');
  }

  async courseCategorySelectLastOption() {
    await this.courseCategorySelect.all(by.tagName('option')).last().click();
  }

  async courseCategorySelectOption(option) {
    await this.courseCategorySelect.sendKeys(option);
  }

  getCourseCategorySelect() {
    return this.courseCategorySelect;
  }

  async getCourseCategorySelectedOption() {
    return this.courseCategorySelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setNameInput('name');
    await waitUntilDisplayed(this.saveButton);
    await this.setDescriptionInput('description');
    await waitUntilDisplayed(this.saveButton);
    await this.setPrerequisiteInput('prerequisite');
    await waitUntilDisplayed(this.saveButton);
    await this.setImageInput(absolutePath);
    await this.courseCategorySelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
  }
}
