import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import CourseCategoryComponentsPage from './course-category.page-object';
import CourseCategoryUpdatePage from './course-category-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('CourseCategory e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let courseCategoryComponentsPage: CourseCategoryComponentsPage;
  let courseCategoryUpdatePage: CourseCategoryUpdatePage;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();
    await signInPage.username.sendKeys(username);
    await signInPage.password.sendKeys(password);
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    courseCategoryComponentsPage = new CourseCategoryComponentsPage();
    courseCategoryComponentsPage = await courseCategoryComponentsPage.goToPage(navBarPage);
  });

  it('should load CourseCategories', async () => {
    expect(await courseCategoryComponentsPage.title.getText()).to.match(/Course Categories/);
    expect(await courseCategoryComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete CourseCategories', async () => {
    const beforeRecordsCount = (await isVisible(courseCategoryComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(courseCategoryComponentsPage.table);
    courseCategoryUpdatePage = await courseCategoryComponentsPage.goToCreateCourseCategory();
    await courseCategoryUpdatePage.enterData();
    expect(await isVisible(courseCategoryUpdatePage.saveButton)).to.be.false;

    expect(await courseCategoryComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(courseCategoryComponentsPage.table);
    await waitUntilCount(courseCategoryComponentsPage.records, beforeRecordsCount + 1);
    expect(await courseCategoryComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await courseCategoryComponentsPage.deleteCourseCategory();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(courseCategoryComponentsPage.records, beforeRecordsCount);
      expect(await courseCategoryComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(courseCategoryComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
