# University portal


## Development

```
npm install
```


Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

```
./mvnw
npm start
```


### Packaging as jar

To build the final jar and optimize the portal application for production, run:

```
./mvnw -Pprod clean verify
```

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

```
java -jar target/*.jar
```

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

`

## Testing

To launch your application's tests, run:

```
./mvnw verify
```



## Using Docker to simplify development (optional)

Launch all your infrastructure by running: `docker-compose up -d`.